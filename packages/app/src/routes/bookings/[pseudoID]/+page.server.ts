import { Selector, loadQuery } from '$lib/zeus';
import type { PageServerLoad } from './$types';
import { error, redirect } from '@sveltejs/kit';

/* @generated from schema by /packages/api/build/scripts/update-id-prefix-to-typename-map.js */
const ID_PREFIXES_TO_TYPENAMES = {
  u: 'User',
  godparentreq: 'GodparentRequest',
  candidate: 'UserCandidate',
  passreset: 'PasswordReset',
  emailchange: 'EmailChange',
  service: 'Service',
  link: 'Link',
  major: 'Major',
  minor: 'Minor',
  school: 'School',
  credential: 'Credential',
  ae: 'StudentAssociation',
  contribution: 'Contribution',
  contributionoption: 'ContributionOption',
  g: 'Group',
  a: 'Article',
  e: 'Event',
  tg: 'TicketGroup',
  t: 'Ticket',
  r: 'Registration',
  log: 'LogEntry',
  lydia: 'LydiaAccount',
  lydiapayment: 'LydiaTransaction',
  barweek: 'BarWeek',
  notifsub: 'NotificationSubscription',
  notif: 'Notification',
  ann: 'Announcement',
  subj: 'Subject',
  doc: 'Document',
  comment: 'Comment',
} as const;
/* end @generated from schema */

function reverseMap<K extends string, V extends string>(obj: Record<K, V>): Record<V, K> {
  return Object.fromEntries(Object.entries(obj).map(([k, v]) => [v, k])) as unknown as Record<V, K>;
}

export const load: PageServerLoad = async ({ fetch, parent, params, url }) => {
  if (params.pseudoID.startsWith(reverseMap(ID_PREFIXES_TO_TYPENAMES).Registration + ':')) {
    throw redirect(
      301,
      url.pathname.replace(params.pseudoID, params.pseudoID.split(':')[1]!.toUpperCase()),
    );
  }

  const id = `${
    reverseMap(ID_PREFIXES_TO_TYPENAMES).Registration
  }:${params.pseudoID.toLowerCase()}`;

  const { registration, registrationQRCode } = await loadQuery(
    {
      registrationQRCode: [{ id }, { path: true, viewbox: true }],
      registration: [
        {
          id,
        },
        Selector('Registration')({
          __typename: true,
          '...on Error': {
            message: true,
          },
          '...on QueryRegistrationSuccess': {
            data: {
              createdAt: true,
              paymentMethod: true,
              id: true,
              beneficiary: true,
              beneficiaryUser: {
                uid: true,
                firstName: true,
                lastName: true,
                fullName: true,
              },
              authorIsBeneficiary: true,
              paid: true,
              opposed: true,
              cancelled: true,
              author: {
                uid: true,
                firstName: true,
                lastName: true,
                fullName: true,
              },
              ticket: {
                price: true,
                name: true,
                group: {
                  name: true,
                },
                links: {
                  computedValue: true,
                  name: true,
                },
                event: {
                  uid: true,
                  title: true,
                  startsAt: true,
                  group: { uid: true },
                },
              },
            },
          },
        }),
      ],
    },
    { fetch, parent },
  );

  if (registration.__typename === 'Error') throw error(400, registration.message);

  return { registration: registration.data, registrationQRCode };
};
